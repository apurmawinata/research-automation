@regression
Feature: As a RESearch user, I want to be able to confirm and validate my website and data

  @login
  Scenario: As a RESearch user, I want to be able to login
    Given I am on RESearch homepage
    And as a user, I fill in my user name as "andri.purmawinata@onthehouse.com.au"
    And as a user, I fill in my password as "portplus123"
    When I click login button
    Then I should be directed to the login page
    And I log out

  @data-update
  Scenario Outline: As a RESearch user, I want to be able to confirm that my data is updated in the last 24hrs
    Given I am at login page
    And I search for "<search> "
    When I check the first "<data>" data is updated
    And I log out
    Examples:
    | search | data |
    |        | 1    |

  @data-update2
  Scenario Outline: As a RESearch user, I want to be able to confirm that my For Sale data is updated within 10 days
    Given I am at login page
    And I search for "<search> "
    And I refine my search to show For Sale listing
    When I check the first "<data>" data is within "11" days
    And I navigate to the next page
    When I check the first "<data>" data is within "11" days
    And I log out
  Examples:
    | search | data |
    |        | 50   |

  @timer
  Scenario: As a RESearch user, I want to be measure my search result loading time
    Given I am at login page
    And I search for " "
    When I check loading time of "10" seconds is acceptable
    And I log out

  @images
  Scenario: As a RESearch user, I want to be able to validate all my images
    Given I am at login page
    And I search for " "
    And I toggle my list view
    And I confirm "10" images are present
    When I check the first "5" images are not broken images
    And I log out

  @count
  Scenario: As a RESearch user, I want to check that number of search properties and pages are updated
    Given I am at login page
    And I search for " "
    And I check my property counts are more than "6500000" in "130000" pages
    And I log out
