
Feature: As a user I want to be able to test data integrity in my showcase site

  @showcase-timer @wip
  Scenario: As a showcase user, I want to measure my rental property loading time
    Given I search rental "6021 Mount Lindesay Hwy, Woodhill, QLD, 4285" in showcase
    When I check "45" seconds loading time for rental showcase is acceptable

  @publisher
  Scenario: As a showcase user, I want to test my data accuracy
    Given I am at login page
    And I search for " "
    And I record the first property
    And I go to showcase page
    When I search the address recorded
    Then I confirm all the details

  @search-showcase
  Scenario Outline: As a showcase user, I want to test my state search result
    And I search "<search>" in showcase site
    And I confirm search "<result>" is in the state
  Examples:
    | search        | result |
    | NSW           | NSW         |
    | QLD           | QLD         |
    | ACT           | ACT         |


