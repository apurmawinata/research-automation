require '.\features\step_definitions\pages\Control_Panel'
require '.\features\step_definitions\pages\Home_Page'

Before do
  p "Initiating scenario..."
  @timeStamp = Time.now.nsec.to_s.freeze
  @today_date = Time.now.strftime('%d %b %Y')
  @showcase = "http://showcase1.opp.othdev.com/"

  @home_page = Home_Page.new
  @control_panel = Control_Panel.new
end

After do
  # page.driver.browser.window_handles.each do |handle|
  #   page.driver.browser.switch_to.window(handle)
  @home_page = nil
  @control_panel = nil
  p "After Scenario "
end

at_exit do
  # exec `ruby report.rb`
  # visit 'http://localhost:8080/'
  GC.start
  p "End of Tests "

end