When /I record the first property/ do
  find("tr.td_alternate:nth-child(99) > td:nth-child(7) > a:nth-child(1) > div:nth-child(1)")
  @publisher_address = all("tr.hover_highlight:nth-child(1) > td:nth-child(2) > a:nth-child(1) > div")[0].text
  @publisher_date = all("tr.hover_highlight:nth-child(1) > td:nth-child(7) > a:nth-child(1) > div")[0].text
  @publisher_price = all("tr.hover_highlight:nth-child(1) > td:nth-child(6) > a:nth-child(1) > div")[0].text
end

When /I go to showcase page/ do
  visit @showcase
  has_title? "Publisher Platform"
  end

When /I search the address recorded/ do
  find(".form-control").set @publisher_address
  find(".c-search-button").click
  end

When /I confirm all the details/ do
  #confirm address, price, etc
  @publisher_price.should include (find(".c-property-price__price").text)
  @publisher_address.downcase.should include (all(".property-summary__street-address")[0].text.downcase)
  @publisher_date.downcase.should include (all(".property-summary__sold-date")[0].text.downcase)
end

When /I search for "(.*)"/ do |keyword|
  @control_panel.quick_search keyword

  if keyword == "NSW"
    results = find("div.autocomplete-suggestion", :match => :first).text
    results.downcase == 'AARONS PASS, NSW, 2850'.downcase
  end
  find("#autocomplete_submit_btn").click
  @start = Time.now.to_f
end

When /I refine my search to show For Sale listing/ do
  find(".sprite-square-plus").click
  find("#isForSale").click
  find("#refine_search_button").click
  find(".listingDate > span:nth-child(1)").text.should == "Listing Date"
end

When /^I search "(.*)" in showcase site$/ do |state|
  visit @showcase
  has_title? "Publisher Platform"

  #search for a state
  find(".form-control").set state
  find(".c-search-button").click
  end

When /^I confirm search "(.*)" is in the state$/ do |state|
  find("div.c-search-element:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(2) > span:nth-child(2)").text.downcase.should == state.downcase
end

When /I check my property counts are more than "(.*)" in "(.*)" pages/ do |property_numbers, page_numbers|
  stats = find(".search_statistics").text
  match1 = stats.match(/\d+.\d+.\d+/)
  prop_results = match1.to_s.gsub(/\D/, "").to_i
  prop_results.should > property_numbers.to_i

  match2 = stats.match(/\(\d+.\d+.\d+/)
  page_results = match2.to_s.gsub(/\D/, "").to_i
  page_results.should > page_numbers.to_i

end

Given /^I toggle my list view$/ do
  find(".expand-icon").click
end

Given /^I check the first "(.*)" images are not broken images$/ do | number |
  @broken = 0
  for x in (0..(number.to_i - 1))
    if all(".property_thumbnail")[x].native.attribute("src") =~ /https:\/\/research.othsolutions.com.au\/assets\/images\/OTH_House_illustration_\S+/
      @broken = @broken + 1
    end
  end
  @broken.should < number.to_i
end

Given /^I confirm "(.*)" images are present$/ do | image_number |
  for x in (0..(image_number.to_i - 1))
  # puts "Image URL number #{x} " + all(".property_thumbnail")[x].native.attribute("src")

  begin
  @result1 = all(".property_thumbnail")[x].native.attribute("src").should =~ /https:\/\/\S+\/resize\/oth\/property_photos\/\S+height=72/
  rescue Exception => e
    # puts e.to_s.red
  end

  begin
  @result2 = all(".property_thumbnail")[x].native.attribute("src").should =~ /https:\/\/maps.googleapis.com\/maps\/api\/streetview\?size=102x72&location=\S+onthehouse&signature\S+/
  rescue Exception => e
    # puts e.to_s.red
  end

  begin
    @result3 = all(".property_thumbnail")[x].native.attribute("src").should =~ /https:\/\/research.othsolutions.com.au\/assets\/images\/OTH_House_illustration_\S+/
  rescue Exception => e
    # puts e.to_s.red
  end

  @result = @result1 || @result2 || @result3
  @result.should == true

  end
end

When /I check loading time of "(.*)" seconds is acceptable/ do |time|
  find("tr.td_alternate:nth-child(99) > td:nth-child(7) > a:nth-child(1) > div:nth-child(1)")
  duration = timer_calc(@start)
  # puts "Loading Time " + duration.to_s
  duration.should < time.to_f
end

Given /^I analyze$/ do
  analyze
end

Given /^I click on the first suburb$/ do
  find(".ui-autocomplete .ui-corner-all", :match => :first).click
end

When /^I search rental "(.*)" in showcase$/ do |keyword|
  visit @showcase
  has_title? "Publisher Platform"

  #search for a state
  find(".form-control").set keyword
  find(".c-search-button").click
  @start = Time.now.to_f
end

When /I check "(.*)" seconds loading time for rental showcase is acceptable/ do |time|
  # wait_for_css(".property-summary__street-address", "sleep 0.01")
  find(".property-summary__street-address")
  find(".c-property-price__price")
  duration = timer_calc(@start)
  # puts "Loading Time " + duration.to_s
  duration.should < time.to_f
end


