Given /^I am on RESearch homepage$/ do
  @home_page.visit_page $base_url
  @home_page.check_title
end

When /^as a user, I fill in my user name as "(.*?)"$/ do |userName|
  @home_page.fill_in_username userName
end

When /^as a user, I fill in my password as "(.*?)"$/ do |password|
  @home_page.fill_in_password password
end

When /^I navigate to the next page$/ do

  begin
    find("div.pagination-block:nth-child(12) > a:nth-child(1)").click
  rescue Exception => e
    # puts e.to_s.red
  end
  begin
    find("div.pagination-block:nth-child(11) > a:nth-child(1)").click
  rescue Exception => e
    # puts e.to_s.red
  end
end

When /^I click login button$/ do
  @home_page.click_login
end

When /^I log out$/ do
  find("a.no-icon:nth-child(2)").click
end

When /^I should be directed to the login page$/ do
  page.current_url.should include 'https://research.othsolutions.com.au'
  @control_panel.check_title
end

When /^I check the first "(.*)" data is updated$/ do | amount |
  (has_title? 'Property search - RESearch - Best Source of Actual Real Estate Data').should == true
  x = 1
  while x <= (amount.to_i)*2 - 1 do
    require 'active_support/all'
    sold_date = all("tr.hover_highlight:nth-child(#{x}) > td:nth-child(7) > a:nth-child(1) > div")[0].text
    date_check = Date.parse(sold_date)
    today = Date.today.strftime('%d %b %Y')
    within_3_days = (Date.today-2).strftime('%d %b %Y')
    puts "property css #{x} " + sold_date
    # date_check.between?(Date.parse(within_2_days),Date.parse(today)).should == true

    if (date_check.between?(Date.parse(within_3_days),Date.parse(today))) == false
    raise "The last sold date of #{date_check} is outdated, expecting sold date to be within 3 days"
    end
    x = x + 2
  end
  end

When /^I check the first "(.*)" data is within "(.*)" days$/ do | amount, days |
  (has_title? 'Property search - RESearch - Best Source of Actual Real Estate Data').should == true
  x = 1
  find("tr.hover_highlight:nth-child(99) > td:nth-child(7) > a:nth-child(1) > div")
  while x <= (amount.to_i)*2 - 1 do
    require 'active_support/all'
    list_date = all("tr.hover_highlight:nth-child(#{x}) > td:nth-child(7) > a:nth-child(1) > div")[0].text
    date_check = Date.parse(list_date)
    today = Date.today.strftime('%d %b %Y')
    within_x_days = (Date.today-((days.to_i)-1)).strftime('%d %b %Y')
    puts "property css #{x} " + list_date + " can't be earlier than " + within_x_days
    if (date_check.between?(Date.parse(within_x_days),Date.parse(today))) == false
      raise "The listing date of some properties is on #{list_date}, expecting it to be within #{days} days"
    end
    x = x + 2
  end

end

Given /^I am at login page$/ do
  steps %{
    Given I am on RESearch homepage
    And as a user, I fill in my user name as "andri.purmawinata@onthehouse.com.au"
    And as a user, I fill in my password as "portplus123"
    When I click login button
    Then I should be directed to the login page
}
end

